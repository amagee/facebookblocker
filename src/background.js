import { getBlockedWebsites, isRunning, tick } from './store.js';

window.setInterval(function() {
  const blockedWebsites = getBlockedWebsites();
  tick();
  if (isRunning()) {
    chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
      if (tabs.length > 0) {
        var tab = tabs[0];
        var withoutProtocol = removeProtocol(tab.url);
        var blockedWebsite = blockedWebsites.filter(w => withoutProtocol.startsWith(w))[0];
        if (blockedWebsite != null) {
          chrome.tabs.update(tab.id, {
            url: chrome.extension.getURL('blocked.html?blocked=' + blockedWebsite)
          });
        }
      }
    });
  }
}, 500);

function removeProtocol(url) {
  return url.replace(/^[a-z]+:\/\//, '');
}


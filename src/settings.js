import React from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment';
import { getSettings, saveSettings } from './store.js';


const buttonStyle = {
  fontSize: '18px',
  padding: '0.4rem',
  letterSpacing: '1px',
  color: '#fff',
  border: 0,
  background: '#2433bb',
  borderRadius: '4px',
  textAlign: 'center'
};



var Content = React.createClass({
  getInitialState: function() {
    return {
      ...getSettings(),
      justSaved: false
    };
  },

  render: function() {
    return <div>
      <h3 style={{fontSize: '1.4em', padding: '1em', backgroundColor: '#eeeef5'}}>
        Focus Switch Settings
      </h3>
      <div style={{padding: '1em'}}>
        <div style={{padding: '1em'}}>
          <div>
            <h3>Websites to block:</h3>
            <ul style={{marginTop: '0.6em', listStyleType: 'disc', listStylePosition: 'inside', marginLeft: '0.5em'}}>
              <li style={{marginBottom: '0.3em'}}>
                Enter websites to block, one website per line.
              </li>
              <li style={{marginBottom: '0.3em'}}>
                Entering <code>example.com</code> will block <code>example.com</code>, 
                {' '}<code>www.example.com</code>, <code>timesink.example.com</code> etc.
              </li>
            </ul>
            <textarea
              rows={8}
              cols={55}
              value={this.state.websitesText}
              onChange={(event) => this.setState({websitesText: event.target.value})}
              style={{marginTop: '0.6em'}}
            />
          </div>
          <div style={{marginTop: '1.6em'}}>
            {'Block for '}
            <input 
              type="number"
              value={this.state.blockMinutes}
              style={{width: '3em', textAlign: 'right'}}
              onChange={(event) => this.setState({blockMinutes: event.target.value})}
            />
            {' minutes'}
          </div>
          <div style={{marginTop: '1.2em'}}>
            <button onClick={this.handleSubmit} style={buttonStyle}>
              Save settings
            </button>
            {this.state.justSaved ? 
              <span style={{marginLeft: '1em'}}>
                Settings saved
              </span>
            : null}
          </div>
        </div>

        <div style={{marginTop: '2em'}}>
          <small>
            {'Icon distributed under '}
            <a target="_blank" href="https://creativecommons.org/licenses/by/3.0/deed.en_US">
              Creative Commons Attribution 3.0 Unported license
            </a>
            {' by '}
            <a target="_blank" href="https://www.vecteezy.com/">
              Vecteezy.com
            </a>.
          </small>
        </div>
      </div>
    </div>;
  },

  handleSubmit: function() {
    let self = this;
    saveSettings({
      websitesText: this.state.websitesText,
      blockMinutes: this.state.blockMinutes
    });
    this.setState({justSaved: true});
    setTimeout(function() {
      self.setState({justSaved: false});
    }, 1500);
  }
});


window.onload = function() {
  ReactDOM.render(<Content />, document.getElementById('content'));
}


import React from 'react';
import ReactDOM from 'react-dom';


const pStyle = {
  lineHeight: '1.3em',
  fontSize: '1rem',
  maxWidth: '30em', 
  marginLeft: 'auto', 
  marginRight: 'auto'
};


var Content = React.createClass({
  propTypes: {
    blockedWebsite: React.PropTypes.string
  },

  getDefaultProps: function() {
    return {
      blockedWebsite: 'this website'
    };
  },

  render: function() {
    return <div>
      <h3 style={{fontSize: '1.4em', padding: '1em', backgroundColor: '#eeeef5'}}>
        Focus Switch Is On!
      </h3>
      <p style={{...pStyle, marginTop: '5em'}}>
        Your focus is so important to us that we sent a team of laser
        sharks to block {this.props.blockedWebsite} from distracting you.
      </p>
      <p style={{...pStyle, marginTop: '1em'}}>
        Don't worry though, we will call off the sharks in time for your next break.
      </p>
    </div>;
  }
});

window.onload = function() {
  const search = window.location.search;
  let blockedWebsite;
  if (search[0] === '?') {
    const params = search.substr(1).split("&").map(function(p) {
      const [key, val] = p.split("=");
      return [decodeURIComponent(key), decodeURIComponent(val)];
    });
    const param = params.filter(([key]) => key === 'blocked')[0];
    if (param != null) {
      blockedWebsite = param[1];
    }
  }

  ReactDOM.render(
    <Content blockedWebsite={blockedWebsite} />, 
    document.getElementById('content')
  );
}


import moment from 'moment';


export function initialize() {
  if (window.localStorage.blockedWebsites == null) {
    window.localStorage.setItem('blockedWebsites',
      [
        'www.facebook.com',
        'twitter.com'
      ].join("\n")
    );
  }
  if (window.localStorage.blockMinutes == null) {
    window.localStorage.blockMinutes = 30;
  }
}

export function start() {
  const now = +new Date();
  const finishesAt = now + (window.localStorage.blockMinutes * 60 * 1000);
  window.localStorage.setItem('startedAt', now);
  window.localStorage.setItem('finishesAt', finishesAt);
  window.localStorage.setItem('previousFinishType', null);
  window.localStorage.setItem('previousStartedAt', null);
  window.localStorage.setItem('previousFinishedAt', null);
}

function _stop(stopType) {
  window.localStorage.setItem('previousFinishType', stopType);
  window.localStorage.setItem('previousStartedAt', window.localStorage.startedAt);
  window.localStorage.setItem('previousFinishedAt', +new Date());
  window.localStorage.setItem('finishesAt', null);
}

export function stop() {
  _stop('stopped');
}

export function tick() {
  const finishesAt = window.localStorage.finishesAt;
  if (finishesAt != null && finishesAt !== 'null' && (+new Date()) > parseInt(window.localStorage.finishesAt)) {
    _stop('complete');
  }
}

export function isRunning() {
  const finishesAt = window.localStorage.finishesAt;
  if (finishesAt == null || finishesAt === 'null') {
    return false;
  }
  return parseInt(finishesAt) > +new Date();
}

export function saveSettings({websitesText, blockMinutes}) {
  window.localStorage.setItem('blockedWebsites', websitesText);
  window.localStorage.setItem('blockMinutes', blockMinutes);
}

export function getSettings() {
  initialize();
  return {
    websitesText: window.localStorage.blockedWebsites,
    blockMinutes: window.localStorage.blockMinutes
  };
}

export function getBlockedWebsites() {
  return (getSettings()
    .websitesText
    .split("\n")
    .filter(w => w !== '')
    .map(removeProtocol)
  );
}

function parseDateTime(dt) {
  if (dt == null || dt === 'null') {
    return null;
  }
  else {
    return moment(parseInt(dt));
  }
}


function parseNullable(val) {
  if (val == null || val === 'null') {
    return null;
  }
  else {
    return val;
  }
}
    

export function getState() {
  return {
    startedAt: parseDateTime(window.localStorage.startedAt),
    finishesAt: parseDateTime(window.localStorage.finishesAt),
    previousFinishType: parseNullable(window.localStorage.previousFinishType),
    previousStartedAt: parseDateTime(window.localStorage.previousStartedAt),
    previousFinishedAt: parseDateTime(window.localStorage.previousFinishedAt),
  };
}


function removeProtocol(url) {
  return url.replace(/^[a-z]+:\/\//, '');
}

import React from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment';
import { start, stop, isRunning, getState } from './store.js';

// Link to source for icon
// Pause button is actually a stop button

function to2(n) {
  return n < 10 ? ('0' + n) : n;
}

function getMessage() {
  let message;
  if (isRunning()) {
    const finishDate = new Date(parseInt(window.localStorage.finishesAt));
    const diff = finishDate - +new Date();
    const diffSeconds = diff / 1000;
    return `${to2(Math.floor(diffSeconds / 60))}:${to2(Math.floor(diffSeconds % 60))}`;
  }
  else {
    return "--:--";
  }
}

const size = '3.5rem';

const containerStyle = {
  width: '20em', 
  'height': '6em', 
  backgroundColor: '#eeeef5', 
  display: 'flex', 
  alignItems: 'center', 
  justifyContent: 'center',
  flexDirection: 'column'
};

const panelStyle = {
  flex: '0 0 auto',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  fontSize: '26px'
}

const timerStyle = {
  display: 'inline-flex', 
  alignItems: 'center',
  justifyContent: 'center',
  width: '5.5rem',
  font: '28px monospace',
  marginLeft: '0.5em',
  color: '#272773',
  fontWeight: 600,
  height: size
}

const buttonStyle = {
  display: 'inline-block', 
  width: size, 
  height: size,
  padding: '0.4rem',
  letterSpacing: '1px',
  color: '#fff',
  border: 0,
  background: '#2433bb',
  borderRadius: '4px',
  textAlign: 'center'
};


var Content = React.createClass({
  getInitialState: function() {
    if (window.localStorage.blockMinutes == null) {
      window.localStorage.blockMinutes = 30;
    }

    return {
      ...getState(),
      isRunning: isRunning(),
      message: getMessage(),
      now: moment()
    }
  },

  componentDidMount: function() {
    let self = this;
    this.timer = setInterval(function() {
      self.setState({
        ...getState(),
        isRunning: isRunning(),
        message: getMessage(),
        blockMinutes: window.localStorage.blockMinutes,
        now: moment()
      });
    }, 100);
  },

  componentWillUnmount: function() {
    clearInterval(this.timer);
  },

  render: function() {
    return <div>
      <div style={{backgroundColor: '#f5f5f9', paddingBottom: '0.5em'}}>
        <div style={{padding: '0.5em', textAlign: 'center'}}>
          Focus Switch
        </div>
        <div style={containerStyle}>
          <div style={panelStyle}>
            {this.state.isRunning ?
              <button 
                  style={buttonStyle}
                  onClick={this.handleToggleClick}>
                <img 
                  src="stop.svg"
                  style={{width: '100%', height: '100%'}}
                />
              </button>
            : null}
            {this.state.isRunning ?
              <span style={timerStyle}>
                <span style={{flex: '0 0 auto'}}>
                  {this.state.message}
                </span>
              </span>
            : null}
            {!this.state.isRunning ?
              <button style={{...buttonStyle, fontSize: '22px', width: 'auto'}} onClick={this.handleToggleClick}>
                Focus for {this.state.blockMinutes}m
              </button>
            : null}
          </div>
        </div>
        {this.state.previousFinishType != null ?
          <div style={{marginTop: '0.5em', textAlign: 'center', color: '#3e3e3e'}}>
            <em>
              {'Last '}
              {this.state.previousFinishType === 'completed' ?
                'completed'
              : 'cancelled'}
              {' '}
              {moment.duration(this.state.now.diff(this.state.previousFinishedAt)).humanize()}
              {' ago'}
            </em>
          </div>
        : null}
      </div>
      <div style={{padding: '0.5em', textAlign: 'right'}}>
        <a target="_blank" href="./settings.html">
          Settings
        </a>
      </div>
    </div>;
  },

  handleToggleClick: function() {
    if (this.state.isRunning) {
      stop();
    }
    else {
      start();
    }
  }
});


window.onload = function() {
  ReactDOM.render(<Content />, document.getElementById('content'));
}


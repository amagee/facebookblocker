var webpack = require('webpack');

var buildMode = 'development';
//var buildMode = process.env.BUILD_MODE;
if (buildMode !== 'production' && buildMode !== 'development') {
  throw new Error("BUILD_MODE environment variable must be 'production' or 'development'");
}
var debugMode = process.env.DEBUG_MODE != null ? JSON.parse(process.env.DEBUG_MODE) : null;
if (debugMode == null) {
  debugMode = (buildMode === 'development') ? true : false;
}

console.log("Build mode: ", buildMode);
console.log("Debug mode: ", debugMode);

var config = {
  entry: {
    popup: __dirname + '/src/popup.js',
    settings: __dirname + '/src/settings.js',
    background: __dirname + '/src/background.js',
    blocked: __dirname + '/src/blocked.js',
  },

  output: {
    filename: __dirname + (buildMode === 'production' ? '/build/[name].min.js' : '/build/[name].js')
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['es2015', 'react'],
          plugins: [
            'transform-object-rest-spread',
            
            // This makes static class variables work in IE10.
            'transform-proto-to-assign',

            'transform-decorators-legacy',

            'transform-regenerator',
          ]
        }
      }
    ]
  },

  plugins: debugMode ? 
    []
  : 
    [
      new webpack.DefinePlugin({
        "process.env": { 
          NODE_ENV: JSON.stringify("production")
        }
      })
    ]
};

if (buildMode === 'development') {
  // Source map dev mode
  config.devtool = 'cheap-source-map';
}
else {
  config.devtool = 'source-map';
}

module.exports = config;

